//
//  GeoReminder.h
//  ParkPlus
//

#import <Foundation/Foundation.h>
#import <EventKit/EventKit.h>
#import <CoreLocation/CoreLocation.h>

@interface GeoReminder : UIViewController <CLLocationManagerDelegate>
{
    @protected
    CLLocation *_prevLocation;
}

@property (strong, nonatomic) EKEventStore *eventStore;
@property (strong, nonatomic) CLLocationManager *manager;
@property (strong, nonatomic) CLLocation *location;
@property (strong, readonly, nonatomic) CLLocation *prevLocation;
@property (strong, nonatomic) NSDate *initialTime;
@property (nonatomic, assign) BOOL newCall; // set True when setGeoReminder is called

- (void)setGeoReminder;

@end
