//
//  ViewController.h
//  ParkPlus
//


#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <EventKit/EventKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

#include "GeoReminder.h"

@class GeoReminder;

@interface ViewController : GeoReminder <MFMessageComposeViewControllerDelegate,
                            UINavigationControllerDelegate, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *smsTextField;

- (IBAction)sendSMS:(id)sender;
- (IBAction)showMap:(id)sender;

@end
