//
//  GeoReminder.m
//  ParkPlus
//


#import "GeoReminder.h"

#define proximityRadius 500 // proximity radius 500m before reminder is triggered
#define horAccuracy 100  // Horizontal accuracy of 100m
#define timeToWait 60 // Wait 60s before timing out
#define timeToRetry 5 // try to set reminder at 5s intervals

@implementation GeoReminder

- (void)setGeoReminder;
{
    if (_eventStore == nil)
    {
        // initialize the event store and then request access to the reminders
        _eventStore = [[EKEventStore alloc]init];
        
        [_eventStore requestAccessToEntityType:EKEntityTypeReminder completion:^(BOOL granted, NSError *error) {
            
            if (!granted) {
                NSLog(@"Access to store not granted");
            } else {
                NSLog(@"Access to store granted!");
            }
        }];
    }
    
    // If we have access then init a location manager
    if (_eventStore)
    {
        _manager = [[CLLocationManager alloc]init];
        _manager.delegate = self;
        _manager.distanceFilter = kCLDistanceFilterNone;
        _manager.desiredAccuracy = kCLLocationAccuracyBest;
        
        _newCall = YES;
        [_manager startUpdatingLocation];
        
        // Wait 5 seconds for location updates and then set the reminder
        [NSTimer scheduledTimerWithTimeInterval:timeToRetry target:self selector:@selector(callReminder) userInfo:nil repeats:NO];

    }
    
}

- (void)callReminder;
{
    double timeInterval = fabs([_initialTime timeIntervalSinceNow]);
    
    // If we've been waiting over a minute, then give up if no location has been found.
    if (timeInterval > timeToWait && !_location) {
        [_manager stopUpdatingLocation];
        NSLog(@"Giving up...");
        return;

    // If no location has been found, wait another 5 seconds and try again.
    } else if (!_location) {
        [NSTimer scheduledTimerWithTimeInterval:timeToRetry target:self selector:@selector(callReminder) userInfo:nil repeats:NO];
        NSLog(@"Waiting for location...");
        return;
    }
    
    // Init a new EKStructuredLocation instance and set the current location from the end of the locations array
    EKStructuredLocation *location = [EKStructuredLocation
                                      locationWithTitle:@"Current Location"];
    
     location.geoLocation = _location;
    
    location.radius = proximityRadius; // Radius for proximity
    
    
    // If latest location is within the accuracy and time intervals below, then create a reminder
    if (location.geoLocation.horizontalAccuracy < horAccuracy || timeInterval > timeToWait) {
        
        // Initialize new EkReminder instance and give it a name
        EKReminder *reminder = [EKReminder
                                reminderWithEventStore:_eventStore];
        reminder.title = @"ParkPlus Reminder";
        reminder.notes = @"Don't forget to cancel your ParkPlus parking! Thank you for using ParkPlus.";
        
        // Use the default calendar to store the reminder
        reminder.calendar = [_eventStore defaultCalendarForNewReminders];
        
        // Init a new EKAlarm instance and set to current location, add to reminder
        EKAlarm *alarm = [[EKAlarm alloc]init];
        alarm.structuredLocation = location;
        alarm.proximity = EKAlarmProximityLeave;
        [reminder addAlarm:alarm];
        
        // Update the store with the new reminder
        NSError *error = nil;
        [_eventStore saveReminder:reminder commit:YES error:&error];
        
        if (error) {
            NSLog(@"Failed to set reminder: %@", error);
        } else  if (!error) {
            NSLog(@"Reminder set!");
            // Clear location for next reminder call
            _prevLocation = _location;
            _location = nil;
            // Stop location manager from further location updates
            [_manager stopUpdatingLocation];
        }
    } else {
        [NSTimer scheduledTimerWithTimeInterval:timeToRetry target:self selector:@selector(callReminder) userInfo:nil repeats:NO];
        NSLog(@"Waiting for better accuracy...");
        return;
    }
}

#pragma mark - CLLocationManagerDelegate

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    if (_newCall) {
		_initialTime = [NSDate date];
		_newCall = NO;
	}
    
    _location = [locations lastObject];
    
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error;
{
    
    // If denied access to location services, then stop updating
    if(error.code == kCLErrorDenied) {
        
        NSLog(@"Denied Access to Location Services.");
        [_manager stopUpdatingLocation];
        
    } else if(error.code == kCLErrorLocationUnknown) {
        // retry
        
    } else {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error retrieving location"
                                                        message:[error description]
                                                        delegate:nil
                                                        cancelButtonTitle:@"OK"
                                                        otherButtonTitles:nil];
        [alert show];
    }
}

@end
