//
//  ViewController.m
//  ParkPlus
//


#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (IBAction)sendSMS:(id)sender;
{

    MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
    
    // Check if device supports sending of SMS
    if([MFMessageComposeViewController canSendText]) {
        
        controller.body = _smsTextField.text;
        controller.recipients = [NSArray arrayWithObjects:@"77587", nil];
        controller.messageComposeDelegate = self;
        [self presentViewController:controller animated:YES completion:nil];
        
    // SMS not supported, so alert the user
    } else {
        
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"Sorry"
                                  message:@"Device not configured to send SMS."
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
        
    }
}

- (IBAction)showMap:(id)sender;
{
    CLLocationCoordinate2D coordinate = _prevLocation.coordinate; 
    
    // Create MKMapItem out of coordinates
    MKPlacemark *placeMark = [[MKPlacemark alloc] initWithCoordinate:coordinate addressDictionary:nil];
    MKMapItem *destination =  [[MKMapItem alloc] initWithPlacemark:placeMark];
    
    // Launch iOS6 native maps app
    [destination openInMapsWithLaunchOptions:@{MKLaunchOptionsDirectionsModeKey:MKLaunchOptionsDirectionsModeDriving}];
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result;
{
	switch (result) {
		case MessageComposeResultCancelled:
			NSLog(@"SMS Cancelled");
			break;
            
		case MessageComposeResultFailed:
        {
            NSLog(@"SMS Compose Failed");
            UIAlertView *alertView = [[UIAlertView alloc]
                                      initWithTitle:@"ParkPlus"
                                      message:@"Error, try again."
                                      delegate:self
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil];
            [alertView show];
			break;
        }
            
		case MessageComposeResultSent:
            NSLog(@"SMS Sent Successfully!");
            [self setGeoReminder];
			break;
                                  
		default:
			break;
	}
    
	[self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewDidLoad;
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    // Set the delegate for the UITextField
    [_smsTextField setDelegate:self];

    if ([CLLocationManager regionMonitoringAvailable]) {
        NSLog(@"Region Monitoring Available!");
    } else {
        NSLog(@"Region Monitoring Not Available.");
    }
    
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorized) {
        NSLog(@"Region Monitoring is Authorized!");
    } else {
        NSLog(@"Region Monitoring Not Authorized.");
    }
}

- (void)didReceiveMemoryWarning;
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField;
{
    // close the keyboard on enter
    [textField resignFirstResponder];
    return NO;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField;
{
    // allow editing of text field
    return TRUE;
}

- (void)textFieldDidEndEditing:(UITextField *)textField;
{
    // optionally set state when an edit has completed
}

@end
